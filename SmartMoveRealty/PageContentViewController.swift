//
//  PageContentViewController.swift
//  SmartMoveRealty
//
//  Created by mrinal khullar on 9/17/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class PageContentViewController: UIViewController
{

    @IBOutlet weak var ImageView: UIImageView!
    
    var images:String = String()
    
    var pageIndex:NSInteger = NSInteger()
    
    //var homeImages = ["smartmove_splash1.png","smartmove_splash2.png","smartmove_splash.png"]
    
    
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Compatibility()
        
        self.ImageView.image = UIImage(named: self.images as String)
    }

    
    
    
    //MARK: - Compatibility()
    func Compatibility()
    {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            //ImageView.frame.origin.y = 64
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                //self.pageViewController.view.frame = CGRectMake(0, 70, self.view.frame.width, 300)
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                
            }
                
            else
            {
                
            }
            
        }
        
    }
    

    
    
    
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
