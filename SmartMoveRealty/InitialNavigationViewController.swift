//
//  InitialNavigationViewController.swift
//  SmartMoveRealty
//
//  Created by mrinal khullar on 9/22/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class InitialNavigationViewController: UINavigationController
{

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.interactivePopGestureRecognizer!.enabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
