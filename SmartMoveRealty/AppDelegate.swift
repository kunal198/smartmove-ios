//
//  AppDelegate.swift
//  SmartMoveRealty
//
//  Created by mrinal khullar on 8/24/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

var contact = String()
var urlLink = String()

var navBarTitle = String()

var currentVC:UIViewController = UIViewController()

var postPropertyVC: UIViewController = UIViewController()

var myMenuView :UIView = UIView()
var closeMenuBtn:UIButton = UIButton()

var mainview :UIView = UIView()
var view :UIView = UIView()
var label:UILabel = UILabel()


var barButtonCheck = String()

let dashBoard_btn:UIButton = UIButton()
let advancedSearch_btn:UIButton = UIButton()
let propertyForSale_btn:UIButton = UIButton()
let propertyForRent_btn:UIButton = UIButton()
let postProperty_btn:UIButton = UIButton()


let contactUS_btn:UIButton = UIButton()


let aboutUs_Btn:UIButton = UIButton()
let feedBack_btn:UIButton = UIButton()




var dashBoard_Image : UIImageView = UIImageView()
var advancedSearch_Image : UIImageView = UIImageView()
var propertyForSale_Image : UIImageView = UIImageView()
var propertyForRent_Image : UIImageView = UIImageView()
var postProperty_Image : UIImageView = UIImageView()
var aboutUs_Image : UIImageView = UIImageView()
var feedBack_Image : UIImageView = UIImageView()

var contactUS_Image : UIImageView = UIImageView()


let dashBoardLAbel:UILabel = UILabel()
let advancedSearchLabel:UILabel = UILabel()
let propertyForSaleLabel:UILabel = UILabel()
let propertyForRentLabel:UILabel = UILabel()
let postPropertyLabel:UILabel = UILabel()
let aboutUsLabel:UILabel = UILabel()
let feedBackLabel:UILabel = UILabel()

let contactUSLabel:UILabel = UILabel()


 var x: CGFloat = 0.0

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?
    
    var menu_Label:UILabel = UILabel()
    
    let lineImage: UIImageView = UIImageView()
    
    
    //MARK: 1.DashBoard View Variables
    var dashBoardView :UIView = UIView()
    var dashBoardView_line:UILabel = UILabel()
    
    
    //MARK: 2.About View Variables
    var advancedSearchView :UIView?
    var advanceSearchView_line:UILabel = UILabel()
    
    
    
    //MARK: 4.Resale Properties View Variables
    var propertyForSaleView :UIView?
    var propertyForSaleView_line:UILabel = UILabel()
    
    
    
    //MARK: 5.Exchange Property View Variables
    var propertyForRentView :UIView?
    var propertyForRentView_line:UILabel = UILabel()
    
    
    //MARK: 6.Post Property View Variables
    var postPropertyView :UIView?
    var postPropertyView_line:UILabel = UILabel()
    
    
    
    //MARK: 7.Contact Us View Variables
    var  aboutUsView:UIView?
    var aboutUsView_line:UILabel = UILabel()
    
    
    
    //MARK: 7.Contact Us View Variables
    var  feedBackView:UIView?
    var feedBackView_line:UILabel = UILabel()
    

    
    //MARK: 9.Contact US View Variables
    var  contactUSView:UIView?
    var contactUSView_line:UILabel = UILabel()

    

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        // Override point for customization after application launch.
        
        creatingMenu()
        
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "header-1.png"), forBarMetrics: UIBarMetrics.Default)
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
      //  UINavigationBar.appearance().backItem?.title = "Back"
        
    
        
        // var navBarImage:
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        return true
    }
    
    
    
    
    
    
    
    func creatingMenu()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            myMenuView = UIView(frame:CGRectMake(0, 64,(250/320)*UIScreen.mainScreen().bounds.size.width, (800/568)*UIScreen.mainScreen().bounds.size.height))
            
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                myMenuView = UIView(frame:CGRectMake(0, 0, 250, 568))
                
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                
                myMenuView = UIView(frame: CGRectMake(0, 0, 250, 800))
                
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                myMenuView = UIView(frame: CGRectMake(0, 0, 350, 800))
            }
                
            else
            {
                myMenuView = UIView(frame:CGRectMake(0, 0,320 , 800))
            }
            
            
        }
        
        closeMenuBtn.frame = CGRectMake(0,0,UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height)
        closeMenuBtn.backgroundColor = UIColor.blackColor()
        closeMenuBtn.alpha = 0.5
        closeMenuBtn.hidden = true
        
        window?.addSubview(closeMenuBtn)
        window?.addSubview(myMenuView)
        
        myMenuView.backgroundColor = UIColor.whiteColor()
        
        
        closeMenuBtn.addTarget(self, action:"closeMenuBtnClicked:",forControlEvents: UIControlEvents.TouchUpInside)
        
        //MARK: - 1.MENU View
        
        
        mainview = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (65/568)*UIScreen.mainScreen().bounds.size.height))
        
        mainview.backgroundColor = UIColor (red: 149.0/255, green: 0.0/255, blue: 11.0/255, alpha: 1.0)
        
        
        
        
        myMenuView .addSubview(mainview)
        
        
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            mainview = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (274/320)*UIScreen.mainScreen().bounds.size.width, (65/568)*UIScreen.mainScreen().bounds.size.height))
            
            mainview.backgroundColor = UIColor (red: 149.0/255, green: 0.0/255, blue: 11.0/255, alpha: 1.0)
            
            
            
            
            myMenuView .addSubview(mainview)
            
            
            
            label.frame = CGRectMake((20/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height,(120/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
            
            label.textColor = UIColor.whiteColor()
            label.textAlignment = NSTextAlignment.Left
            label.text = "MENU"
            
            
            label.tag = 0
            
            myMenuView .addSubview(label)
 
        }
        
        label.frame = CGRectMake((20/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height,(100/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Left
        label.text = "MENU"
        
        
        label.tag = 0
        
        myMenuView .addSubview(label)
        
        

        view.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height)
        
        view.backgroundColor = UIColor (red: 149.0/255, green: 0.0/255, blue: 11.0/255, alpha: 1.0)

        
        myMenuView.addSubview(view)

    


        
        
        
        
        //MARK: - 1.Home View
        
        dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (65/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        //dashBoardView.backgroundColor = UIColor.redColor()
        
        
        
        
        myMenuView .addSubview(dashBoardView)
        
        
        dashBoardLAbel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(150/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        dashBoardLAbel.textColor = UIColor (red: 149.0/255, green: 0.0/255, blue: 11.0/255, alpha: 1.0)

        dashBoardLAbel.textAlignment = NSTextAlignment.Left
        dashBoardLAbel.text = "HOME"
        
        
        dashBoardLAbel.tag = 0
        
        dashBoard_btn .addSubview(dashBoardLAbel)
        
        
        //homeLabel.backgroundColor = UIColor.greenColor()
        
        
        dashBoard_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (10/568)*UIScreen.mainScreen().bounds.size.height, (22/568)*UIScreen.mainScreen().bounds.size.height, (20/568)*UIScreen.mainScreen().bounds.size.height))
        dashBoard_Image.image = UIImage(named:"i1.png")
       
        
        dashBoard_btn .addSubview(dashBoard_Image)
        
        
        
        
        dashBoard_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        dashBoard_btn.backgroundColor = UIColor.whiteColor()
        
        dashBoard_btn.setTitle ("", forState: UIControlState.Highlighted)
        
        dashBoard_btn.addTarget(self, action:"goTDashBoardVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        dashBoardView.addSubview(dashBoard_btn)
        
        dashBoard_btn.tag = 0
        
        //dashBoard_btn.backgroundColor = UIColor.blackColor()
        
        
        dashBoardView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(110/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        dashBoardView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(dashBoardView_line)
//        myEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
//        dashBoardLAbel.textColor = UIColor.blackColor()
//        dashBoardLAbel.textAlignment = NSTextAlignment.Left
//        dashBoardLAbel.text = "Dashboard"
//        dashBoardLAbel.font = UIFont.boldSystemFontOfSize(13)
//        
        
    
    

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            dashBoardLAbel.font = UIFont.boldSystemFontOfSize(18)
            dashBoardView_line.frame.size.height = 1
            
            dashBoard_Image.frame.origin.y = 25
            dashBoardLAbel.frame.origin.y = 18
        }
        else
        {
            dashBoardLAbel.font = UIFont.boldSystemFontOfSize(13)
            dashBoardView_line.frame.size.height = 1
            
            dashBoard_Image.frame.origin.y = 16
            dashBoardLAbel.frame.origin.y = 12
        }
        
        
        
        
        
        //MARK: - 2.About View
        advancedSearchView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (120/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        advancedSearchView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(advancedSearchView!)
        
        
        advancedSearch_Image = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (9/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        
        advancedSearch_Image.image = UIImage(named:"i2.png")
        
        advancedSearch_btn.addSubview(advancedSearch_Image)
        
        
        
        advancedSearchLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(150/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        advancedSearchLabel.textColor = UIColor.blackColor()
        advancedSearchLabel.textAlignment = NSTextAlignment.Left
        advancedSearchLabel.text = "ABOUT"
        
        
        
        advancedSearchLabel.tag = 1
        
        
        advancedSearch_btn.addSubview(advancedSearchLabel)
        
        
        
        advancedSearch_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        advancedSearch_btn.backgroundColor = UIColor.whiteColor()
        advancedSearch_btn.setTitle ("", forState: UIControlState.Normal)
        advancedSearch_btn.addTarget(self, action:"goToAdvancedSearchVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        
        // advancedSearch_btn.backgroundColor = UIColor.blackColor()
        
        advancedSearchView!.tag = 95
        
        advancedSearchView?.addSubview(advancedSearch_btn)
        
        advancedSearch_btn.tag = 1
        
        
        
        advanceSearchView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(160/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        advanceSearchView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(advanceSearchView_line)
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            advancedSearchLabel.font = UIFont.boldSystemFontOfSize(18)
            advanceSearchView_line.frame.size.height = 1
        }
        else
        {
            advancedSearchLabel.font = UIFont.boldSystemFontOfSize(13)
            advanceSearchView_line.frame.size.height = 1
        }
        
        
//
        //MARK: - 3.Mega Projects View
        
        propertyForSaleView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (170/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForSaleView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(propertyForSaleView!)
        
        
        
        propertyForSale_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(9/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForSale_Image.image = UIImage(named:"i3.png")
        
        propertyForSale_btn.addSubview(propertyForSale_Image)
        
        
        
        propertyForSale_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForSale_btn.backgroundColor = UIColor.whiteColor()
        
        propertyForSale_btn.setTitle ("", forState: UIControlState.Normal)
        propertyForSale_btn.addTarget(self, action:"goToPropertiesForSaleVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        
        propertyForSaleView?.addSubview(propertyForSale_btn)
        
        propertyForSale_btn.tag = 2
        
        
        
        propertyForSaleLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        propertyForSaleLabel.textColor = UIColor.blackColor()
        propertyForSaleLabel.textAlignment = NSTextAlignment.Left
        propertyForSaleLabel.text = "PROJECTS"
        
        
        
        propertyForSale_btn.addSubview(propertyForSaleLabel)
        
        
        propertyForSale_btn.tag = 2
        
        
        
        
        propertyForSaleView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(210/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForSaleView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(propertyForSaleView_line)
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            propertyForSaleLabel.font = UIFont.boldSystemFontOfSize(18)
            propertyForSaleView_line.frame.size.height = 1
        }
        else
        {
            propertyForSaleLabel.font = UIFont.boldSystemFontOfSize(13)
            propertyForSaleView_line.frame.size.height = 1
        }
        
        
        
//
        //MARK: - 4.Resale Properties View
        propertyForRentView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (220/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForRentView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(propertyForRentView!)
        
        
        
        propertyForRent_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(8/568)*UIScreen.mainScreen().bounds.size.height,  (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        
        
        propertyForRent_Image.image = UIImage(named:"i4.png")
        
        
        propertyForRent_btn.addSubview(propertyForRent_Image)
        
        propertyForRent_btn.tag = 3
        
        
        
        propertyForRentLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(180/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        propertyForRentLabel.textColor = UIColor.blackColor()
        propertyForRentLabel.textAlignment = NSTextAlignment.Left
        propertyForRentLabel.text = "RESALE"
        
        
        
        
        propertyForRent_btn.addSubview(propertyForRentLabel)
        
        
        //resalePropertiesLabel.backgroundColor = UIColor.greenColor()
        
        propertyForRentLabel.tag = 3
        
        
        propertyForRent_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        
        propertyForRent_btn.backgroundColor = UIColor.whiteColor()
        //myEssayButton.textColor = UIColor.blackColor()
        propertyForRent_btn.setTitle ("", forState: UIControlState.Normal)
        propertyForRent_btn.addTarget(self, action:"goToPropertiesOnRentVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        propertyForRentView?.addSubview(propertyForRent_btn)
        
        
        
        
        //propertyForRent_btn.backgroundColor = UIColor.blackColor()
        
        
        propertyForRentView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(260/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForRentView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(propertyForRentView_line)
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            propertyForRentLabel.font = UIFont.boldSystemFontOfSize(18)
            propertyForRentView_line.frame.size.height = 1
        }
        else
        {
            propertyForRentLabel.font = UIFont.boldSystemFontOfSize(13)
            propertyForRentView_line.frame.size.height = 1
        }
        
        
        
//
        //MARK: - 5.Exchange Properties View
        postPropertyView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (270/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        postPropertyView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(postPropertyView!)
        
        
        
        postProperty_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        
        postProperty_Image.image = UIImage(named:"i5.png")
        
        
        postProperty_btn.addSubview(postProperty_Image)
        
        postProperty_btn.tag = 4
        
        
        //exchangePropertyLabel.backgroundColor = UIColor.greenColor()
        
        
        postPropertyLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //MyPlanLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        postPropertyLabel.textColor = UIColor.blackColor()
        postPropertyLabel.textAlignment = NSTextAlignment.Left
        postPropertyLabel.text = "RENT"
        
        
        
        
        postProperty_btn.addSubview(postPropertyLabel)
        
        
        postPropertyLabel.tag = 4
        
        postProperty_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        postProperty_btn.backgroundColor = UIColor.whiteColor()
        //myEssayButton.textColor = UIColor.blackColor()
        postProperty_btn.setTitle ("", forState: UIControlState.Normal)
        postProperty_btn.addTarget(self, action:"goToExchangePropertyVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        postPropertyView?.addSubview(postProperty_btn)
        
        
        
        // postProperty_btn.backgroundColor = UIColor.blackColor()
        
        
        
        postPropertyView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(
            310/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        postPropertyView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(postPropertyView_line)
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            postPropertyLabel.font = UIFont.boldSystemFontOfSize(18)
            postPropertyView_line.frame.size.height = 1
        }
        else
        {
            postPropertyLabel.font = UIFont.boldSystemFontOfSize(13)
            postPropertyView_line.frame.size.height = 1
        }
        
        
//
        //MARK: - 6.Post Property View
        aboutUsView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(320/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        aboutUsView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(aboutUsView!)
        
        
        aboutUs_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        
        aboutUs_Image.image = UIImage(named:"i6.png")
        
        aboutUs_Btn.addSubview(aboutUs_Image)
        
        aboutUs_Btn.tag = 5
        
        
        
        // postPropertyLabel.backgroundColor = UIColor.greenColor()
        
        
        aboutUsLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //myAccountLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        aboutUsLabel.textColor = UIColor.blackColor()
        aboutUsLabel.textAlignment = NSTextAlignment.Left
        aboutUsLabel.text = "EXCHANGE PROPERTY"
        
        aboutUsLabel.tag = 5
        
        
        aboutUs_Btn.addSubview(aboutUsLabel)
        
        
        aboutUs_Btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        aboutUs_Btn.backgroundColor = UIColor.whiteColor()
        aboutUs_Btn.setTitle ("", forState: UIControlState.Normal)
        aboutUs_Btn.addTarget(self, action:"goToAboutUsVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        aboutUsView?.addSubview(aboutUs_Btn)
        
        
        // aboutUs_Btn.backgroundColor = UIColor.blackColor()
        
        
        aboutUsView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(360/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        aboutUsView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(aboutUsView_line)
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            aboutUsLabel.font = UIFont.boldSystemFontOfSize(18)
            aboutUsView_line.frame.size.height = 1
        }
        else
        {
            aboutUsLabel.font = UIFont.boldSystemFontOfSize(13)
            aboutUsView_line.frame.size.height = 1
        }
        
        
        
        
//
        //MARK: - 7.Contact Us View
        feedBackView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(370/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        feedBackView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(feedBackView!)
        
        feedBack_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        feedBack_btn.backgroundColor = UIColor.whiteColor()
        //myEssayButton.textColor = UIColor.blackColor()
        feedBack_btn.setTitle ("", forState: UIControlState.Normal)
        feedBack_btn.addTarget(self, action:"goToFeedBackVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        feedBack_btn.tag = 6
        
        
        
        //feedBack_btn.backgroundColor = UIColor.blackColor()
        
        
        feedBack_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        
        feedBack_Image.image = UIImage(named:"i7.png")
        
        feedBack_btn.addSubview(feedBack_Image)
        
        
        // contactUsView_btn.backgroundColor = UIColor.whiteColor()
        
        feedBackLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(2/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(30/568)*UIScreen.mainScreen().bounds.size.height)
        
        feedBackLabel.textColor = UIColor.blackColor()
        feedBackLabel.textAlignment = NSTextAlignment.Left
        feedBackLabel.text = "POST PROPERTY"
        
        
        feedBack_btn.addSubview(feedBackLabel)
        
        
        feedBackLabel.tag = 6
        
        //contactUsLabel.backgroundColor = UIColor.greenColor()
        
        feedBackView?.addSubview(feedBack_btn)
        
        
        feedBackView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(410/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        feedBackView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(feedBackView_line)
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            feedBackLabel.font = UIFont.boldSystemFontOfSize(18)
            feedBackView_line.frame.size.height = 1
        }
        else
        {
            feedBackLabel.font = UIFont.boldSystemFontOfSize(13)
            feedBackView_line.frame.size.height = 1
        }
        
        
        
        
        
        
        
//
        //MARK: - 7.Contact Us View
        contactUSView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(420/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        contactUSView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(contactUSView!)
        
        contactUS_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        contactUS_btn.backgroundColor = UIColor.whiteColor()
        //myEssayButton.textColor = UIColor.blackColor()
        contactUS_btn.setTitle ("", forState: UIControlState.Normal)
        contactUS_btn.addTarget(self, action:"goToCOntactsVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        contactUS_btn.tag = 7
        
        
        
        
        
        
        contactUS_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        
        contactUS_Image.image = UIImage(named:"i7.png")
        
        contactUS_btn.addSubview(contactUS_Image)
        
        
        // contactUsView_btn.backgroundColor = UIColor.whiteColor()
        
        
        
        contactUSLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(2/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(30/568)*UIScreen.mainScreen().bounds.size.height)
        
        contactUSLabel.textColor = UIColor.blackColor()
        contactUSLabel.textAlignment = NSTextAlignment.Left
        contactUSLabel.text = "CONTACT US"
        
        
        contactUS_btn.addSubview(contactUSLabel)
        
        
        contactUSLabel.tag = 7
        
        //contactUsLabel.backgroundColor = UIColor.greenColor()
        
        contactUSView?.addSubview(contactUS_btn)
        
        
        contactUSView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(460/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        contactUSView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(contactUSView_line)
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            contactUSLabel.font = UIFont.boldSystemFontOfSize(18)
            contactUSView_line.frame.size.height = 1
        }
        else
        {
            contactUSLabel.font = UIFont.boldSystemFontOfSize(13)
            contactUSView_line.frame.size.height = 1
        }
        
//
    }
    
    
    
    func goTDashBoardVC(sender: UIButton!)
    {
        highLightBtn(sender)
        closeMenuBtn.hidden = true
        
        print("dashboard button clicked", terminator: "")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let dashBoardVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
        //let navigationItem = UINavigationItem()
        dashBoardVC.contact = "HOME"
        
       // barButtonCheck = "2"
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        urlLink = "http://smartmoverealty.in/?mobile=true"
        
        currentVC.navigationController?.pushViewController(dashBoardVC, animated: true)
    }
    
    
    
    
    func goToAdvancedSearchVC(sender: UIButton!)
    {
        highLightBtn(sender)
        closeMenuBtn.hidden = true
        
        print("advance search view button clicked", terminator: "")
        
        
        
        navBarTitle = "ABOUT"
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let aboutUsVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
        aboutUsVC.contact = "ABOUT"
        
        
        urlLink = "http://smartmoverealty.in/about/?mobile=true"
        
                //barButtonCheck = "1"
        
        currentVC.navigationController?.pushViewController(aboutUsVC, animated: true)
    }
    
    
    
    
    func goToPropertiesForSaleVC(sender: UIButton!)
    {
        highLightBtn(sender)
        closeMenuBtn.hidden = true
        
        
        
        print("property for sale button clicked", terminator: "")
        
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
    let propertyOnSaleVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
                //barButtonCheck = "1"
        
                //saleCheck = "sale_menu"
        
        
        navBarTitle = "PROJECTS"
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        
        propertyOnSaleVC.contact = "PROJECTS"
        
        urlLink = "http://smartmoverealty.in/projects/?mobile=true"
        
        currentVC.navigationController?.pushViewController(propertyOnSaleVC, animated: true)
    }
    
    
    
    func goToPropertiesOnRentVC(sender: UIButton!)
    {
        highLightBtn(sender)
        closeMenuBtn.hidden = true
        
        print("property on rent button clicked", terminator: "")
        
        
        
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
                let propertiesOnRentVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
               // barButtonCheck = "1"
        
        
        navBarTitle = "RESALE"
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
         propertiesOnRentVC.contact = "RESALE"
        urlLink = "http://smartmoverealty.in/resale/?mobile=true"
        
                currentVC.navigationController?.pushViewController(propertiesOnRentVC, animated: true)
    }
    
    
    func goToExchangePropertyVC(sender: UIButton!)
    {
        highLightBtn(sender)
        closeMenuBtn.hidden = true
        
        print("Exchange property view button clicked", terminator: "")
        
        
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
                let exchangePropertyVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
               // barButtonCheck = "1"
        
        
        navBarTitle = "RENT"
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        exchangePropertyVC.contact = "RENT"
        
         urlLink = "http://smartmoverealty.in/rent/?mobile=true"
        
                currentVC.navigationController?.pushViewController(exchangePropertyVC, animated: true)
    }
    
    
    func goToAboutUsVC(sender: UIButton!)
    {
        
        
        print("about us view clicked", terminator: "")
        
        highLightBtn(sender)
        closeMenuBtn.hidden = true
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
       let postPropertyVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
        //print(postPropertyVC)
        
        navBarTitle = "EXCHANGE PROPERTY"
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        postPropertyVC.contact = "EXCHANGE PROPERTY"
        
        urlLink = "http://smartmoverealty.in/exchange-property/?mobile=true"
        
        currentVC.navigationController?.pushViewController(postPropertyVC, animated: true)
        
    }
    
    
    func goToFeedBackVC(sender: UIButton!)
    {
        
        
        print("feedback button clicked", terminator: "")
        
        highLightBtn(sender)
        closeMenuBtn.hidden = true
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let contactVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
        
        navBarTitle = "POST PROPERTY"
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        contactVC.contact = "POST PROPERTY"
        
        urlLink = "http://smartmoverealty.in/post-property/?mobile=true"
        
        currentVC.navigationController?.pushViewController(contactVC, animated: true)
    }
    
    
    
    
    
    
    
    
    func goToCOntactsVC(sender: UIButton!)
    {
        
        print("contacts view button clicked", terminator: "")
        
        highLightBtn(sender)
        closeMenuBtn.hidden = true
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let contactVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
        navBarTitle = "CONTACT US"
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        contactVC.contact = "CONTACT US"
        
        urlLink = "http://smartmoverealty.in/contact-us/?mobile=true"
        
        currentVC.navigationController?.pushViewController(contactVC, animated: true)
    }
    
    
    
    
    
    
    func highLightBtn(sender: UIButton)
    {
        
        var images = [String]()
        images = ["i11.png", "i2.png","i3.png","i4.png","i5.png","i6.png","i7.png","i6.png"]
        
        var images_hover = [String]()
        images_hover = ["i1.png", "i22.png","i33.png","i44.png","i55.png","i66.png","i77.png","i66.png"]
        
        
        var images_icon = [UIImageView]()
        images_icon = [dashBoard_Image,advancedSearch_Image,propertyForSale_Image,propertyForRent_Image,postProperty_Image,aboutUs_Image,feedBack_Image,contactUS_Image]
        
        
        var menu_lbl = [UILabel]()
        menu_lbl = [dashBoardLAbel,advancedSearchLabel,propertyForSaleLabel,propertyForRentLabel,postPropertyLabel,aboutUsLabel,feedBackLabel,contactUSLabel]
        
        for vw in myMenuView.subviews //as! [UIView]
        {
            
            for svw in vw.subviews //as! [UIView]
            {
                if svw.isKindOfClass(UIButton)
                {
                    if svw as NSObject == sender
                    {
                        
                        
                        print("hover images", terminator: "")
                        
                        print(images_hover[svw.tag], terminator: "")
                        
                        images_icon[svw.tag].image = UIImage(named: images_hover[svw.tag] as String)
                        
                        menu_lbl[svw.tag].textColor = UIColor(red: 187/255, green: 31/255, blue: 36/255, alpha: 1.0)
                        
                        //svw.tintColor = UIColor.redColor()
                        
                        //svw.backgroundColor = UIColor(red: 159.0/255 , green: 36.0/255, blue: 41.0/255, alpha: 0.7)
                        
                    }
                    else
                    {
                        //svw.backgroundColor = UIColor.clearColor()
                        
                        print("images icon", terminator: "")
                        
                        //svw.tintColor = UIColor.blackColor()
                        
                        images_icon[svw.tag].image = UIImage(named: images[svw.tag] as String)
                        
                        menu_lbl[svw.tag].textColor = UIColor.blackColor()
                        
                    }
                }
            }
        }
        
    }
    
    
    
    
    func closeMenuBtnClicked(sender: UIButton)
    {
        print("menu btn pressed", terminator: "")
        sender.hidden = true
        
        x = -myMenuView.frame.size.width
        
        UIView.animateWithDuration(0.5, animations: {
            
            // self.navBar.frame.origin.x = self.navx
            myMenuView.frame.origin.x = -myMenuView.frame.size.width
            
        })
        
    }
    
    
    

    
    
    
    
    
    
    
    

    //(320/414)*UIScreen.mainScreen().bounds.size.width
    
//    func creatingMenu()
//    {
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
//        {
//            myMenuView = UIView(frame:CGRectMake(0, 64,(250/320)*UIScreen.mainScreen().bounds.size.width, (800/568)*UIScreen.mainScreen().bounds.size.height))
//            
//        }
//        else
//        {
//            if (UIScreen.mainScreen().bounds.size.height == 568)
//            {
//                myMenuView = UIView(frame:CGRectMake(0, 64, 250, 568))
//                
//            }
//                
//            else if(UIScreen.mainScreen().bounds.size.height == 480)
//            {
//                
//                myMenuView = UIView(frame: CGRectMake(0, 55, 250, 800))
//
//            }
//                
//            else if(UIScreen.mainScreen().bounds.size.height == 736)
//            {
//                myMenuView = UIView(frame: CGRectMake(0, 64, 350, 800))
//            }
//                
//            else
//            {
//                myMenuView = UIView(frame:CGRectMake(0, 65,320 , 800))
//            }
//            
//            
//        }
//        
//        closeMenuBtn.frame = CGRectMake(0,0,UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height)
//        closeMenuBtn.backgroundColor = UIColor.blackColor()
//        closeMenuBtn.alpha = 0.5
//        closeMenuBtn.hidden = true
//        
//        window?.addSubview(closeMenuBtn)
//        window?.addSubview(myMenuView)
//        
//        myMenuView.backgroundColor = UIColor.whiteColor()
//        
//        
//        closeMenuBtn.addTarget(self, action:"closeMenuBtnClicked:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        
//
//        //MARK: - 1.Home View
//        dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        //dashBoardView.backgroundColor = UIColor.redColor()
//        
//        
//        
//        
//        myMenuView .addSubview(dashBoardView)
//        
//               
//        dashBoardLAbel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        dashBoardLAbel.textColor = UIColor.blackColor()
//        dashBoardLAbel.textAlignment = NSTextAlignment.Left
//        dashBoardLAbel.text = "Dashboard"
//        
//        
//        dashBoardLAbel.tag = 0
//        
//        dashBoard_btn .addSubview(dashBoardLAbel)
//        
//
//        //homeLabel.backgroundColor = UIColor.greenColor()
//        
//        
//        dashBoard_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (10/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
//        dashBoard_Image.image = UIImage(named:"i11.png")
//        
//        
//        dashBoard_btn .addSubview(dashBoard_Image)
//        
//        
//        
//        
//        dashBoard_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
//        
//       dashBoard_btn.backgroundColor = UIColor.whiteColor()
//        
//        dashBoard_btn.setTitle ("", forState: UIControlState.Normal)
//        dashBoard_btn.addTarget(self, action:"goTDashBoardVC:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        dashBoardView.addSubview(dashBoard_btn)
//        
//        dashBoard_btn.tag = 0
//
//       //dashBoard_btn.backgroundColor = UIColor.blackColor()
//        
//        
//        dashBoardView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(42/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        dashBoardView_line.backgroundColor = UIColor.grayColor()
//        
//        myMenuView.addSubview(dashBoardView_line)
//        //myEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
//        //dashBoardLAbel.textColor = UIColor.blackColor()
//        //dashBoardLAbel.textAlignment = NSTextAlignment.Left
//        //dashBoardLAbel.text = "Dashboard"
//        //dashBoardLAbel.font = UIFont.boldSystemFontOfSize(13)
//        
//        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
//        {
//            dashBoardLAbel.font = UIFont.boldSystemFontOfSize(18)
//            dashBoardView_line.frame.size.height = 1
//            
//            dashBoard_Image.frame.origin.y = 25
//            dashBoardLAbel.frame.origin.y = 18
//        }
//        else
//        {
//            dashBoardLAbel.font = UIFont.boldSystemFontOfSize(13)
//            dashBoardView_line.frame.size.height = 1
//            
//            dashBoard_Image.frame.origin.y = 16
//            dashBoardLAbel.frame.origin.y = 12
//        }
//        
//
//        
//        
//        
//        //MARK: - 2.About View
//        advancedSearchView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (45/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        advancedSearchView!.backgroundColor = UIColor.whiteColor()
//        
//        myMenuView.addSubview(advancedSearchView!)
//        
//        
//        advancedSearch_Image = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (9/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        advancedSearch_Image.image = UIImage(named:"i2.png")
//        
//        advancedSearch_btn.addSubview(advancedSearch_Image)
//        
//        
//        
//        advancedSearchLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(150/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
//        advancedSearchLabel.textColor = UIColor.blackColor()
//        advancedSearchLabel.textAlignment = NSTextAlignment.Left
//        advancedSearchLabel.text = "Advance Search"
//        
//
//        
//        advancedSearchLabel.tag = 1
//        
//        
//        advancedSearch_btn.addSubview(advancedSearchLabel)
//        
//        
//        
//        advancedSearch_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        advancedSearch_btn.backgroundColor = UIColor.whiteColor()
//        advancedSearch_btn.setTitle ("", forState: UIControlState.Normal)
//        advancedSearch_btn.addTarget(self, action:"goToAdvancedSearchVC:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        
//        
//        
//       // advancedSearch_btn.backgroundColor = UIColor.blackColor()
//        
//        advancedSearchView!.tag = 95
//        
//        advancedSearchView?.addSubview(advancedSearch_btn)
//        
//        advancedSearch_btn.tag = 1
//        
//        
//        
//        advanceSearchView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(82/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        advanceSearchView_line.backgroundColor = UIColor.grayColor()
//        
//        myMenuView.addSubview(advanceSearchView_line)
//        
//        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
//        {
//            advancedSearchLabel.font = UIFont.boldSystemFontOfSize(18)
//            advanceSearchView_line.frame.size.height = 1
//        }
//        else
//        {
//            advancedSearchLabel.font = UIFont.boldSystemFontOfSize(13)
//            advanceSearchView_line.frame.size.height = 1
//        }
//
//        
//        
//        //MARK: - 3.Mega Projects View
//        propertyForSaleView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (85/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        propertyForSaleView!.backgroundColor = UIColor.whiteColor()
//        
//        myMenuView.addSubview(propertyForSaleView!)
//        
//        
//        
//        propertyForSale_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(9/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        propertyForSale_Image.image = UIImage(named:"i3.png")
//        
//        propertyForSale_btn.addSubview(propertyForSale_Image)
//        
//        
//        
//        propertyForSale_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        propertyForSale_btn.backgroundColor = UIColor.whiteColor()
//        
//        propertyForSale_btn.setTitle ("", forState: UIControlState.Normal)
//        propertyForSale_btn.addTarget(self, action:"goToPropertiesForSaleVC:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        
//        
//        
//        propertyForSaleView?.addSubview(propertyForSale_btn)
//
//        propertyForSale_btn.tag = 2
//        
//        
//        
//        propertyForSaleLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
//        propertyForSaleLabel.textColor = UIColor.blackColor()
//        propertyForSaleLabel.textAlignment = NSTextAlignment.Left
//        propertyForSaleLabel.text = "Properties for Sale"
//        
//        
//        
//        propertyForSale_btn.addSubview(propertyForSaleLabel)
//        
//        
//        propertyForSale_btn.tag = 2
//        
//        
//        
//        
//        propertyForSaleView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(122/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        propertyForSaleView_line.backgroundColor = UIColor.grayColor()
//        
//        myMenuView.addSubview(propertyForSaleView_line)
//
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
//        {
//            propertyForSaleLabel.font = UIFont.boldSystemFontOfSize(18)
//            propertyForSaleView_line.frame.size.height = 1
//        }
//        else
//        {
//            propertyForSaleLabel.font = UIFont.boldSystemFontOfSize(13)
//            propertyForSaleView_line.frame.size.height = 1
//        }
//
//       
//        
// 
//       //MARK: - 4.Resale Properties View
//        propertyForRentView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (125/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        propertyForRentView!.backgroundColor = UIColor.whiteColor()
//        
//        myMenuView.addSubview(propertyForRentView!)
//        
//        
//        
//        propertyForRent_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(8/568)*UIScreen.mainScreen().bounds.size.height,  (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        
//        propertyForRent_Image.image = UIImage(named:"i4.png")
//        
//        
//        propertyForRent_btn.addSubview(propertyForRent_Image)
//        
//        propertyForRent_btn.tag = 3
//        
//        
//        
//        propertyForRentLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(180/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
//        propertyForRentLabel.textColor = UIColor.blackColor()
//        propertyForRentLabel.textAlignment = NSTextAlignment.Left
//        propertyForRentLabel.text = "Properties for Rent"
//        
//        
//       
//        
//        propertyForRent_btn.addSubview(propertyForRentLabel)
//        
//        
//        //resalePropertiesLabel.backgroundColor = UIColor.greenColor()
//        
//          propertyForRentLabel.tag = 3
//        
//        
//        propertyForRent_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        
//        propertyForRent_btn.backgroundColor = UIColor.whiteColor()
//        //myEssayButton.textColor = UIColor.blackColor()
//        propertyForRent_btn.setTitle ("", forState: UIControlState.Normal)
//        propertyForRent_btn.addTarget(self, action:"goToPropertiesOnRentVC:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        
//        propertyForRentView?.addSubview(propertyForRent_btn)
//        
//        
//        
//        
//        //propertyForRent_btn.backgroundColor = UIColor.blackColor()
//        
//        
//        propertyForRentView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(162/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        propertyForRentView_line.backgroundColor = UIColor.grayColor()
//        
//        myMenuView.addSubview(propertyForRentView_line)
//
//        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
//        {
//            propertyForRentLabel.font = UIFont.boldSystemFontOfSize(18)
//            propertyForRentView_line.frame.size.height = 1
//        }
//        else
//        {
//            propertyForRentLabel.font = UIFont.boldSystemFontOfSize(13)
//            propertyForRentView_line.frame.size.height = 1
//        }
//        
//
//        
//        
//        //MARK: - 5.Exchange Properties View
//        postPropertyView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (165/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        postPropertyView!.backgroundColor = UIColor.whiteColor()
//        
//        myMenuView.addSubview(postPropertyView!)
//        
//        
//        
//        postProperty_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        postProperty_Image.image = UIImage(named:"i5.png")
//        
//        
//        postProperty_btn.addSubview(postProperty_Image)
//        
//        postProperty_btn.tag = 4
//        
//        
//        //exchangePropertyLabel.backgroundColor = UIColor.greenColor()
//        
//        
//        postPropertyLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        //MyPlanLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
//        postPropertyLabel.textColor = UIColor.blackColor()
//        postPropertyLabel.textAlignment = NSTextAlignment.Left
//        postPropertyLabel.text = "Post Property"
//        
//
//        
//        
//        postProperty_btn.addSubview(postPropertyLabel)
//        
//        
//        postPropertyLabel.tag = 4
//        
//        postProperty_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        postProperty_btn.backgroundColor = UIColor.whiteColor()
//        //myEssayButton.textColor = UIColor.blackColor()
//        postProperty_btn.setTitle ("", forState: UIControlState.Normal)
//        postProperty_btn.addTarget(self, action:"goToExchangePropertyVC:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        
//        postPropertyView?.addSubview(postProperty_btn)
//       
//        
//        
//       // postProperty_btn.backgroundColor = UIColor.blackColor()
//        
//        
//        
//        postPropertyView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(202/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        postPropertyView_line.backgroundColor = UIColor.grayColor()
//        
//        myMenuView.addSubview(postPropertyView_line)
//        
//        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
//        {
//            postPropertyLabel.font = UIFont.boldSystemFontOfSize(18)
//            postPropertyView_line.frame.size.height = 1
//        }
//        else
//        {
//            postPropertyLabel.font = UIFont.boldSystemFontOfSize(13)
//            postPropertyView_line.frame.size.height = 1
//        }
//
//
//        
//        //MARK: - 6.Post Property View
//        aboutUsView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(205/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        aboutUsView!.backgroundColor = UIColor.whiteColor()
//        
//        myMenuView.addSubview(aboutUsView!)
//        
//        
//        aboutUs_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        aboutUs_Image.image = UIImage(named:"i6.png")
//        
//        aboutUs_Btn.addSubview(aboutUs_Image)
//        
//        aboutUs_Btn.tag = 5
//        
//        
//        
//       // postPropertyLabel.backgroundColor = UIColor.greenColor()
//        
//        
//        aboutUsLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        //myAccountLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
//        aboutUsLabel.textColor = UIColor.blackColor()
//        aboutUsLabel.textAlignment = NSTextAlignment.Left
//        aboutUsLabel.text = "About us"
//        
//        aboutUsLabel.tag = 5
//        
//        
//        aboutUs_Btn.addSubview(aboutUsLabel)
//        
//        
//        aboutUs_Btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        aboutUs_Btn.backgroundColor = UIColor.whiteColor()
//        aboutUs_Btn.setTitle ("", forState: UIControlState.Normal)
//        aboutUs_Btn.addTarget(self, action:"goToAboutUsVC:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        
//        aboutUsView?.addSubview(aboutUs_Btn)
//        
//        
//       // aboutUs_Btn.backgroundColor = UIColor.blackColor()
//        
//        
//        aboutUsView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(242/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        aboutUsView_line.backgroundColor = UIColor.grayColor()
//        
//        myMenuView.addSubview(aboutUsView_line)
//
//        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
//        {
//            aboutUsLabel.font = UIFont.boldSystemFontOfSize(18)
//            aboutUsView_line.frame.size.height = 1
//        }
//        else
//        {
//            aboutUsLabel.font = UIFont.boldSystemFontOfSize(13)
//            aboutUsView_line.frame.size.height = 1
//        }
//        
//
//        
//        
//        
//       //MARK: - 7.Contact Us View
//        feedBackView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(245/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        feedBackView!.backgroundColor = UIColor.whiteColor()
//        
//        myMenuView.addSubview(feedBackView!)
//        
//        feedBack_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        feedBack_btn.backgroundColor = UIColor.whiteColor()
//        //myEssayButton.textColor = UIColor.blackColor()
//        feedBack_btn.setTitle ("", forState: UIControlState.Normal)
//        feedBack_btn.addTarget(self, action:"goToFeedBackVC:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        feedBack_btn.tag = 6
//        
//
//        
//        //feedBack_btn.backgroundColor = UIColor.blackColor()
//        
//        
//        feedBack_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
//        
//        feedBack_Image.image = UIImage(named:"i7.png")
//        
//        feedBack_btn.addSubview(feedBack_Image)
//        
//        
//       // contactUsView_btn.backgroundColor = UIColor.whiteColor()
//        
//        feedBackLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(2/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(30/568)*UIScreen.mainScreen().bounds.size.height)
//
//        feedBackLabel.textColor = UIColor.blackColor()
//        feedBackLabel.textAlignment = NSTextAlignment.Left
//        feedBackLabel.text = "Feedback"
//        
//
//        feedBack_btn.addSubview(feedBackLabel)
//        
//        
//         feedBackLabel.tag = 6
//        
//        //contactUsLabel.backgroundColor = UIColor.greenColor()
//        
//       feedBackView?.addSubview(feedBack_btn)
//        
//        
//        feedBackView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(282/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
//        
//        feedBackView_line.backgroundColor = UIColor.grayColor()
//        
//        myMenuView.addSubview(feedBackView_line)
//        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
//        {
//            feedBackLabel.font = UIFont.boldSystemFontOfSize(18)
//            feedBackView_line.frame.size.height = 1
//        }
//        else
//        {
//            feedBackLabel.font = UIFont.boldSystemFontOfSize(13)
//            feedBackView_line.frame.size.height = 1
//        }
//
//       
//    }
//    
//    
//    
//    func goTDashBoardVC(sender: UIButton!)
//    {
//        highLightBtn(sender)
//        closeMenuBtn.hidden = true
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        let dashBoardVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("DashBoardRootViewControllerSegue") as! DashBoardRootViewController
//        
//        currentVC.navigationController?.pushViewController(dashBoardVC, animated: true)
//    }
//    
//    
//    
//    
//    func goToAdvancedSearchVC(sender: UIButton!)
//    {
//        highLightBtn(sender)
//        closeMenuBtn.hidden = true
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        let aboutUsVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("AdvanceSearchViewControllerSegue") as! AdvanceSearchViewController
//        
//        barButtonCheck = "1"
//        
//        currentVC.navigationController?.pushViewController(aboutUsVC, animated: true)
//    }
//    
//    
//    func goToPropertiesForSaleVC(sender: UIButton!)
//    {
//        highLightBtn(sender)
//        closeMenuBtn.hidden = true
//        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        let propertyOnSaleVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("PropertyOnSaleViewControllerSegue") as! PropertyOnSaleViewController
//        
//        barButtonCheck = "1"
//        
//        saleCheck = "sale_menu"
//        
//        currentVC.navigationController?.pushViewController(propertyOnSaleVC, animated: true)
//    }
//    
//    
//
//    func goToPropertiesOnRentVC(sender: UIButton!)
//    {
//        highLightBtn(sender)
//        closeMenuBtn.hidden = true
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        let propertiesOnRentVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("MultiStoreyAppartmentsListViewControllerSegue") as! MultiStoreyAppartmentsListViewController
//        
//        barButtonCheck = "1"
//        
//        currentVC.navigationController?.pushViewController(propertiesOnRentVC, animated: true)
//    }
//    
//    
//    func goToExchangePropertyVC(sender: UIButton!)
//    {
//        highLightBtn(sender)
//        closeMenuBtn.hidden = true
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        let exchangePropertyVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("PostPropertyViewControllerSegue") as! PostPropertyViewController
//        
//        barButtonCheck = "1"
//        
//        currentVC.navigationController?.pushViewController(exchangePropertyVC, animated: true)
//    }
//    
//    
//    func goToAboutUsVC(sender: UIButton!)
//    {
//        highLightBtn(sender)
//        closeMenuBtn.hidden = true
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        postPropertyVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("ExpandAboutUsViewControllerSegue") as! ExpandAboutUsViewController
//        
//        print(postPropertyVC)
//        
//        currentVC.navigationController?.pushViewController(postPropertyVC, animated: true)
//
//    }
//    
//    
//    func goToFeedBackVC(sender: UIButton!)
//    {
//        highLightBtn(sender)
//        closeMenuBtn.hidden = true
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        let contactVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("FeedBackViewControllerSegue") as! FeedBackViewController
//        
//        currentVC.navigationController?.pushViewController(contactVC, animated: true)
//    }
//    
//
//    
//    
//    func highLightBtn(sender: UIButton)
//    {
//        
//        var images = [String]()
//        images = ["i11.png", "i2.png","i3.png","i4.png","i5.png","i6.png","i7.png"]
//        
//        var images_hover = [String]()
//        images_hover = ["i1.png", "i22.png","i33.png","i44.png","i55.png","i66.png","i77.png"]
//        
//        
//        var images_icon = [UIImageView]()
//        images_icon = [dashBoard_Image,advancedSearch_Image,propertyForSale_Image,propertyForRent_Image,postProperty_Image,aboutUs_Image,feedBack_Image]
//        
//
//        var menu_lbl = [UILabel]()
//        menu_lbl = [dashBoardLAbel,advancedSearchLabel,propertyForSaleLabel,propertyForRentLabel,postPropertyLabel,aboutUsLabel,feedBackLabel]
//        
//            for vw in myMenuView.subviews as! [UIView]
//            {
//                
//                for svw in vw.subviews as! [UIView]
//                {
//                    if svw.isKindOfClass(UIButton)
//                    {
//                        if svw == sender
//                        {
//                            
//                            
//                            print("hover images")
//                            
//                            print(images_hover[svw.tag])
//                            
//                            images_icon[svw.tag].image = UIImage(named: images_hover[svw.tag] as String)
//                            
//                            menu_lbl[svw.tag].textColor = UIColor(red: 187/255, green: 31/255, blue: 36/255, alpha: 1.0)
//                            
//                            //svw.tintColor = UIColor.redColor()
//                            
//                            //svw.backgroundColor = UIColor(red: 159.0/255 , green: 36.0/255, blue: 41.0/255, alpha: 0.7)
//                            
//                        }
//                        else
//                        {
//                            //svw.backgroundColor = UIColor.clearColor()
//                            
//                            print("images icon")
//                            
//                            //svw.tintColor = UIColor.blackColor()
//                            
//                            images_icon[svw.tag].image = UIImage(named: images[svw.tag] as String)
//                            
//                            menu_lbl[svw.tag].textColor = UIColor.blackColor()
//                            
//                        }
//                    }
//                }
//            }
//        
//    }
//    
//    
//    
//
//    func closeMenuBtnClicked(sender: UIButton)
//    {
//        print("menu btn pressed")
//        sender.hidden = true
//        
//        x = -myMenuView.frame.size.width
//        
//        UIView.animateWithDuration(0.5, animations: {
//            
//            // self.navBar.frame.origin.x = self.navx
//             myMenuView.frame.origin.x = -myMenuView.frame.size.width
//            
//        })
//        
//    }
//
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

