//
//  DashBoardPageContentViewController.swift
//  SmartMoveRealty
//
//  Created by mrinal khullar on 9/18/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class DashBoardPageContentViewController: UIViewController
{

  
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    
   
    
    
    var images:String = String()
    
    var headingText:String = String()
    
    var DescriptionText:String = String()
    
    var pageIndex:NSInteger = NSInteger()
    
    var imagesArray = ["img_newsfeed.png","img_newsfeed.png","img_newsfeed.png","img_newsfeed.png"]
    
   var titleArray = ["HUDA seeks upkeep charges for new sectors.","HUDA seeks upkeep charges for new sectors.","HUDA seeks upkeep charges for new sectors.","HUDA seeks upkeep charges for new sectors."]
    
    var textViewContent = ["GURGAON HUDA administrator Anita Yadav has sought a new policy for collecting maintainennce charges from the new sectors.58-115 and that the handling over of maintainence services of all old sectors fromv1 to 57 to the MCG be expedited.","GURGAON HUDA administrator Anita Yadav has sought a new policy for collecting maintainennce charges from the new sectors.58-115 and that the handling over of maintainence services of all old sectors fromv1 to 57 to the MCG be expedited.","GURGAON HUDA administrator Anita Yadav has sought a new policy for collecting maintainennce charges from the new sectors.58-115 and that the handling over of maintainence services of all old sectors fromv1 to 57 to the MCG be expedited.","GURGAON HUDA administrator Anita Yadav has sought a new policy for collecting maintainennce charges from the new sectors.58-115 and that the handling over of maintainence services of all old sectors fromv1 to 57 to the MCG be expedited."]
    
    
    
    
   
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Compatibility()
        
        self.ImageView.image = UIImage(named: self.images as String)
        
        self.titleLabel.text = self.headingText as String
        
        self.textView.text = self.DescriptionText as String
        
        self.textView.scrollRangeToVisible(NSMakeRange(0, 0))
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
            textView.font = UIFont.systemFontOfSize(18)

        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 9)
                textView.font = UIFont.systemFontOfSize(10)
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 9)
                textView.font = UIFont.systemFontOfSize(10)
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 12)
                textView.font = UIFont.systemFontOfSize(11)
            }
            else
            {
                titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 12)
                textView.font = UIFont.systemFontOfSize(11)
            }
        }
   }

    
    
    //MARK: - Compatibility()
    func Compatibility()
    {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            //ImageView.frame.origin.y = 64
            
            ImageView.frame.size.width = 180
            ImageView.frame.size.height = 180
            
            titleLabel.frame.origin.x = 220
            titleLabel.frame.size.width = 300
            
            textView.frame.origin.x = 213
            textView.frame.size.width =  500
            textView.frame.size.height =  150
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                titleLabel.frame.origin.x = 130
                textView.frame.origin.x = 125
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                ImageView.frame.size.height = 120
                ImageView.frame.size.width = 110
                
                titleLabel.frame.origin.x = 130
                textView.frame.origin.x = 125

                //self.pageViewController.view.frame = CGRectMake(0, 70, self.view.frame.width, 300)
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                titleLabel.frame.size.width = 220
                textView.frame.size.width = 220
            }
                
            else
            {
                titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
                textView.font = UIFont.systemFontOfSize(12)
                
                titleLabel.frame.size.width = 200
                textView.frame.size.width = 200
            }
            
        }
        
    }

    
    
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
