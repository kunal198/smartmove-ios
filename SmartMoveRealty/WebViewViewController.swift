//
//  WebViewViewController.swift
//  SmartMoveRealty
//
//  Created by mrinal khullar on 1/7/16.
//  Copyright (c) 2016 mrinal khullar. All rights reserved.
//

import UIKit


class WebViewViewController: UIViewController
{
    
    @IBOutlet var MENUIMAGE: UIImageView!
    
    
    var contact = ""
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var navTItle: UILabel!
    var menuBtnClicked:Bool = Bool()
    var navx: CGFloat = 0.0
    var x: CGFloat = 0.0
    
    let navButton = UIButton()
    let navBtnItem = UIBarButtonItem()
    
    
    
    var spinningIndicator : MBProgressHUD = MBProgressHUD()
    
    
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        createStatusBar()
        
        loadWebRequest()
        
        leftBarButton()
        
        
    }
    
    
    
    
    
    
    
    //MARK: - viewWillAppear()
    override func viewWillAppear(animated: Bool)
    {
        
        
        if barButtonCheck == "1"
        {
            self.navigationItem.hidesBackButton = true
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: UIBarButtonItemStyle.Bordered, target: self, action: "menu_Btn:")
        }
        else
        {
            self.navigationItem.hidesBackButton = false
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    //MARK: - createStatusBar()
    func createStatusBar()
    {
        let statusBarView: UIView = UIView()
        
        statusBarView.frame = CGRectMake(0, -20, self.view.frame.size.width, 20)
        
        statusBarView.backgroundColor = UIColor(red: 159.0/255 , green: 0/255, blue: 22.0/255, alpha: 1.0)
        
        self.navigationController?.navigationBar.addSubview(statusBarView)
        
    
        
    }
    
    
    
    
    
    //MARK: - loadWebRequest()
    func loadWebRequest()
    {
        
        //UIWebView.loadRequest(webView)(NSURLRequest(URL: NSURL(string: "www.google.in")!))
        
        
        var url = urlLink as String
        
        if url == ""
        {
            url = "http://smartmoverealty.in/?mobile=true"
            
            contact = "HOME"
            
            navTItle.text = contact
            
        }
        
        
        let requestURL = NSURL(string:url)
        
        let request = NSURLRequest(URL: requestURL!)
        
        webView.loadRequest(request)
        
        print("title for menu = \(contact)")
        
        navTItle.text = contact
        
        
        spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = "Loading"
        
        menuBtn.enabled = false
        
        
    }
    
    
    
    
    //MARK: - leftBarButton()
    func leftBarButton()
    {
        
        navButton.setImage(UIImage(named: "menu"), forState: .Normal)
        navButton.frame = CGRectMake(0, 0, 30, 30)
        navButton.addTarget(self, action: Selector("menu_Btn:"), forControlEvents: .TouchUpInside)
        
        
        navBtnItem.customView = navButton
        
        //        let btn2 = UIButton()
        //        btn2.setImage(UIImage(named: "img2"), forState: .Normal)
        //        btn2.frame = CGRectMake(0, 0, 30, 30)
        //        btn2.addTarget(self, action: Selector("action2:"), forControlEvents: .TouchUpInside)
        //        let item2 = UIBarButtonItem()
        //        item2.customView = btn2
        
        //self.navigationItem.rightBarButtonItems = [item1,item2]
        
        self.navigationItem.leftBarButtonItem = navBtnItem
        
        
        
        
        
        currentVC = self
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        
    }
    
    
    
    //MARK: - menu_Btn()

    @IBAction func menu_Btn(sender: AnyObject)
    {
        
        
        print("menu btn pressed", terminator: "")
        
        
        menuBtnClicked = true
        
        if menuBtnClicked == true
        {
            //searchTextField.resignFirstResponder()
        }
        
        
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
            x = -myMenuView.frame.size.width
            navx = 0
            closeMenuBtn.hidden = true
        }
        
        
        UIView.animateWithDuration(0.5, animations: {
            
            // self.navBar.frame.origin.x = self.navx
            myMenuView.frame.origin.x = self.x
            
        })
        
 
    }
    
    
    
    
    
    
    
    
    //MARK: - UIWebView Delegate
    
    func webViewDidStartLoad(webView: UIWebView)
    {
        // myActivityIndicator.startAnimating()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
    }
    
    
    
    func webViewDidFinishLoad(webView: UIWebView)
    {
        // myActivityIndicator.stopAnimating()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        
        spinningIndicator.hide(true)
        
        menuBtn.enabled = true
    }
    
    
    
    
    
    @IBAction func refreshButtonTapped(sender: AnyObject)
    {
        //myWebView.reload()
    }
    
    

    
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
    
    