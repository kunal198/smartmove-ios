//
//  RootViewController.swift
//  SmartMoveRealty
//
//  Created by mrinal khullar on 9/17/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class RootViewController: UIViewController, UIPageViewControllerDataSource
{
    
    @IBOutlet var getstartedbtn: UIButton!
    @IBOutlet weak var skipThis_lbl: UIButton!
    
    @IBOutlet weak var statusBarView: UIView!
    
    @IBOutlet weak var navBarBack_Image: UIImageView!

    @IBOutlet weak var navBarView: UIView!
    
    var pageViewController:UIPageViewController = UIPageViewController()
    var homeImages:NSArray = NSArray()
    
    var pageControl:UIPageControl = UIPageControl()
    
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        getstartedbtn.backgroundColor = UIColor.whiteColor()
        getstartedbtn.layer.cornerRadius = 5
        getstartedbtn.layer.borderWidth = 1
        getstartedbtn.layer.borderColor = UIColor.whiteColor().CGColor
        
        self.navigationController?.navigationBarHidden = true
        
        Compatibility()
        
        homeImages = ["11.png","13.png","12.png"]
       
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            skipThis_lbl.frame.origin.x = 500
            skipThis_lbl.frame.size.width = 250
            skipThis_lbl.frame.origin.y = 5
            //skipThis_lbl.backgroundColor = UIColor.blackColor()
            skipThis_lbl.imageEdgeInsets = UIEdgeInsetsMake(0, 190, 0, 0)
            skipThis_lbl.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            
            skipThis_lbl.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            
        }
        
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                skipThis_lbl.imageEdgeInsets = UIEdgeInsetsMake(0, 115, 0, 0)
                skipThis_lbl.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20)
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                skipThis_lbl.imageEdgeInsets = UIEdgeInsetsMake(0, 115, 0, 0)
                skipThis_lbl.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20)
                
                navBarView.frame.size.height = 44
                navBarBack_Image.frame.size.height = 44
                //self.pageViewController.view.frame = CGRectMake(0, 60, self.view.frame.width, 200)
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                skipThis_lbl.frame.origin.y = 5
                
                skipThis_lbl.imageEdgeInsets = UIEdgeInsetsMake(0, 140, 0, 0)
                skipThis_lbl.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
                
                skipThis_lbl.frame.size.height = 34
                
                print("skip this button height = \(skipThis_lbl.frame.size.height)", terminator: "")
                
                //skipThis_lbl.backgroundColor = UIColor.grayColor()
                
                skipThis_lbl.titleLabel?.font = UIFont.boldSystemFontOfSize(18)
                
                skipThis_lbl.imageView?.frame.size.height = 10
                skipThis_lbl.imageView?.frame.size.width = 10
            }
                
            else
            {
                skipThis_lbl.frame.origin.y = 0
                skipThis_lbl.imageEdgeInsets = UIEdgeInsetsMake(0, 125, 0, 0)
                skipThis_lbl.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            }

        }
       
        
        //var pageControl_y = self.pageControl.frame.origin.y + self.pageControl.frame.size.height
//        
//        pageControl = UIPageControl.appearance()
//        pageControl.pageIndicatorTintColor = UIColor(red: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 0.5)
//        pageControl.currentPageIndicatorTintColor = UIColor.whiteColor()
//        pageControl.backgroundColor = UIColor(red: 169.0/255, green: 12.0/255, blue: 28.0/255, alpha: 1.0)
//      
    
        
        
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        self.pageViewController.dataSource = self
        
        
        
        let startVC = self.viewControllerAtIndex(0) as PageContentViewController
        let viewControllers = NSArray(object: startVC)
       
        self.pageViewController.setViewControllers(viewControllers as! [UIViewController], direction: .Forward, animated: true, completion: nil)
        
        if(UIScreen.mainScreen().bounds.size.height == 480)
        {
            self.pageViewController.view.frame = CGRectMake(0, 40, self.view.frame.width, 400)
            
            
        }
        else
        {
            self.pageViewController.view.frame = CGRectMake(5, 60, self.view.frame.width-10, 450)
        }
       
        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            self.pageViewController.view.frame = CGRectMake(0, 50, self.view.frame.width, 540)
        }
//        else
//        {
//            self.pageViewController.view.frame = CGRectMake(0, 100, self.view.frame.width, 435)
//        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            self.pageViewController.view.frame = CGRectMake(0, 64, self.view.frame.width, 800)
            
        }
        
        
        
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
    }

    
    
    
    
    
    //MARK: - UIPageViewController() DataSource Methods
    func viewControllerAtIndex(index: Int) -> PageContentViewController
    {
        let vc: PageContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageContentViewControllerSegue") as! PageContentViewController
        
        vc.images = homeImages[index] as! String
       
        vc.pageIndex = index
        
        return vc
    }
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        
        let vc = viewController as! PageContentViewController
        var index = vc.pageIndex as Int
        
        
        if (index == 0 || index == NSNotFound)
        {
            return nil
            
        }
        
        index--
         //[self.pageControl setCurrentPage:index];
        
        //self.pageControl.currentPage = index
        
        return self.viewControllerAtIndex(index)
        
    }
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        let vc = viewController as! PageContentViewController
        
        var index = vc.pageIndex as Int
        
        
        if (index == NSNotFound)
        {
            return nil
        }
        
        
        
        index++
        
        //self.pageControl.currentPage = index
        
        if (index == homeImages.count)
        {
            return nil
        }
    
        
        return self.viewControllerAtIndex(index)
    }
    
    
    
    
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return homeImages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return 0
    }
    
    
    
    
    
    
    //MARK: - skipThis_btn()
    @IBAction func skipThis_btn(sender: AnyObject)
    {
        print("skip this button clicked", terminator: "")
        
        let webViewVC = self.storyboard!.instantiateViewControllerWithIdentifier("WebViewViewControllerSegue") as! WebViewViewController
        
        self.navigationController?.pushViewController(webViewVC, animated: true)

    }
    
    
    
    
    
    
    
    //MARK: - Compatibility()
    func Compatibility()
    {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            
            navBarBack_Image.frame.origin.y = 20
            navBarView.frame.origin.y = 20
            
            navBarView.frame.size.height = 44
            navBarBack_Image.frame.size.height = 44
            statusBarView.frame.size.height = 20
            
            
           
            
           // self.pageViewController.view.frame = CGRectMake(0, 150, self.view.frame.width, 800)

        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
               self.pageViewController.view.frame = CGRectMake(0, 64, self.view.frame.width, 300)
                navBarView.frame.size.height = 44
                navBarBack_Image.frame.size.height = 44
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                skipThis_lbl.imageEdgeInsets = UIEdgeInsetsMake(0, 200, 0, 0)
                skipThis_lbl.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
                
                print(skipThis_lbl.frame.size.width, terminator: "")
                
                print(skipThis_lbl.frame.size.height, terminator: "")
            }
                
            else
            {
                skipThis_lbl.imageEdgeInsets = UIEdgeInsetsMake(0, 250, 0, 0)
                skipThis_lbl.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            }
            
        }
        
    }
    

    
    
    
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
